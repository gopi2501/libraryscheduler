var mongoose = require('mongoose');

var DisasterSchema = mongoose.Schema({
    disastername:{
        type:String,
        required:true
    },
    disasterdropdown: {
        type: String
    },
    contactperson: {
        type: String
    },
    location: {
        type: String
    },
    latitude: {
        type: String
    },
    longitude:{
        type:String
    },
    dateandtime: {
        type: String
    }
});

// module.exports=mongoose.model('eocschema', EocSchema);
module.exports = mongoose.model('disasterschema', DisasterSchema);