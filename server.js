var http = require('http');
var path = require('path');
var express = require('express');
var ejs=require('ejs');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
const flash = require('express-flash');
//var MongoStore = require('connect-mongo')(session);
var mongoose = require('mongoose');
var app = express();

var routes=require('./server/routes');


app.locals.pretty = true;
app.set('port', process.env.PORT || 4456);
// app.set('views', __dirname + '/app/server/views');

// my new code
app.set('views', path.join(__dirname, 'views'));



// end of my code
app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('stylus').middleware({ src: __dirname + '/app/public' }));
app.use(express.static(__dirname + '/public'));



// var dbURL='mongodb://'+dbHost+':'+dbPort+'/'+dbName;
var dbURL = 'mongodb://localhost:27017/edmone';
mongoose.connect(dbURL);
// require('./app/server/routes')(app);
// require('./app/server/routes');

console.log('before calling routes');
app.use('/', routes);

http.createServer(app).listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
});
