var eocmodel = require('../models/eocmodel');
var disastermodel = require('../models/disastermodel');
var express = require('express')
var router = express.Router();
var currentuser;
// module.exports = function (app) {
router.get('/', function (req, res) {
    console.log('in get / ');
    // app.get('/', function (req, res) {
    res.render('login', {
        warningmessage1: ""
    });

});

router.post('/', function (req, res) {
    console.log('post of login page');
    var email = req.body.email;
    console.log('email entered', email);
    currentuser = eocmodel.find({ email: email });
    var password = req.body.password;
    eocmodel.find({ email: email, password: password }, function (err, docs) {
        if (!docs.length) {
            console.log('username obtained is' + docs.username);
            res.render('login', {
                warningmessage1: "invalid email or password !!"
            });
            // window.alert('Username does not exists');
            console.log('username does not exists');
        } else {
            console.log('user exists', docs);
            //    res.send(docs);
            if (docs.length > 0) {
                console.log('the lastname is ', docs[0].lastname);
                res.render('eocdashboard', {
                    myname: docs[0].lastname
                });
            }
        }
    });
});

router.get('/eocregistration', function (req, res) {
    // app.get('/studentregister', function(req,res){
    // document.getElementById('username').innerHTML=currentuser;
    res.render('eocregistration');
});

router.post('/eocregistration', function (req, res) {
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var homeaddress = req.body.homeaddress;
    var homecity = req.body.homecity;
    var homestate = req.body.homestate;
    var homezip = req.body.homezip;
    var officeaddress = req.body.officeaddress;
    var officecity = req.body.officecity;
    var officestate = req.body.officestate;
    var officezip = req.body.officezip;
    var title = req.body.title;
    var mobilenumber = req.body.mobilenumber;
    var homenumber = req.body.homenumber;
    var officenumber = req.body.officenumber;
    var email = req.body.email;
    var password = req.body.password;
    // var checks = req.body.getElementsByClassName('certificationchecks');
    // var listofcert = "";
    // for (var init = 0; init < 9; init++) {
    //     if (checks[init].checked === true) {
    //         listofcert += checks[init].value + ',';
    //     }
    // }

    var neweocmember = new eocmodel({
        firstname: firstname, lastname: lastname,
        homeaddress: {
            address: homeaddress, city: homecity, state: homestate, zip: homezip
        },
        officeAddress: {
            address: officeaddress, city: officecity, state: officestate, zip: officezip
        },
        title: title,
        phonenumber: { mobile: mobilenumber, home: homenumber, office: officenumber },
        email: email, password: password,
        // certifications: listofcert
    });
    console.log('in routes.js before calling create user');

    neweocmember.save(function (err, result) {
        if (err) console.log(err);
        else {
            console.log('successfully inserted an eoc members record');
            // res.send('user added successfully');
            res.render('login', {
                warningmessage1: "Registration Successful. Please login"
            });
        }
    });
});

// router.get('/registeradisaster', function (req, res) {
//     // var disasterdata = setTimeout(disasterpopulater, 5000);
//    var disasterdata=disasterpopulater();
//     console.log("after data from disaster populater "+disasterdata);
//     // res.send(CreateTableFromJSON(disasterdata));
//     res.render('registeradisaster', {
//         registerationmessage: "",
//         disasterdata:disasterdata
//         // CreateTableFromJSON(disasterdata)
//     });
// });

//useful link: https://stackoverflow.com/questions/24592760/how-can-i-pass-an-array-to-an-ejs-template-in-express

router.get('/registeradisaster', function (req, res) {
    disastermodel.find({}, function (err, docs) {
        if (err) {
            console.log('no registered disaster');
        } else {
            console.log('before redirecting to registeradisaster', docs);
            res.render('registeradisaster', {
                registerationmessage: " sdf",
                disasterdata: docs,
                disastertext: docs,
                errormessage: "",
                tablefunction: CreateTableFromJSON
            });
        }
    });
});
// function disasterpopulater() {
//     var alldisasters=new Array();
//     console.log("call for disaster populater");
//     // var alldisasters = disastermodel.find({contactperson:"Aswini"});
//     // disastermodel.find({ contactperson: "Aswini" }, function (err, docs) {
//     disastermodel.find({}, function (err, docs) {
//         if (!docs.length) {
//             console.log('no disasters registerd');

//         } else {
//             alldisasters = docs;
//             console.log('printing all disasters in disasterpopulater', alldisasters);
//             // CreateTableFromJSON();
//             // console.log(docs);
//             return alldisasters;
//         }
//     })

// }



router.post('/registeradisaster', function (req, res) {
    var dropdown = req.body.disasterdropdown;
    console.log('disasterdropdownvalue is', dropdown);
    var disastername = req.body.disastername;
    var contactperson = req.body.contactperson;
    var location = req.body.location;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var dateandtime = req.body.dateandtime;
    var newdisaster = new disastermodel({
        disastername: disastername,
        disasterdropdown: dropdown,
        contactperson: contactperson,
        location: location,
        latitude: latitude,
        longitude: longitude,
        dateandtime: dateandtime
    });

    newdisaster.save(function (err, result) {
        if (err) {
            console.log(err);
            res.render('registeradisaster', {
                registerationmessage: "registration failed",
                errormessage: ""
            });
        } else {
            // res.redirect('/registeradisaster');
            disastermodel.find({}, function (err, docs) {
                if (err) {
                    console.log('no registered disaster');
                } else {
                    console.log('before redirecting to registeradisaster', docs);
                    res.render('registeradisaster', {
                        registerationmessage: " sdf",
                        disasterdata: docs,
                        disastertext: docs,
                        errormessage: "",
                        tablefunction: CreateTableFromJSON
                    });
                }
            });
        }
    })
})

router.get('/forgotpassword', function (req, res) {
    res.render('forgot');
});

router.get('/eocdashboard', function (req, res) {
    res.render('eocdashboard', {
        myname: ""
    });
});

router.get('/blanktemplate', function (req, res) {
    res.render('blank');
});

router.get('/dashboard', function (req, res) {
    res.render('dashboard', {
        myname: currentuser
    });
});

router.get('/login', function (req, res) {
    res.render('login', {
        warningmessage1: "",

    });
});

router.get('/trialpage', function (req, res) {

    res.render('trialpage');

});

router.get('/trialtable', function (req, res) {
    res.render('trialtables');
});

router.get('/disasterpage_:disastername', function (req, res) {

    var disastername = req.params.disastername;
    disastermodel.findOne({ disastername: disastername }, function (err, docs) {
        if (err) {
            res.render('registeradisaster', {
                errormessage: "Error while extracting from the database"
            });
        } else {
            console.log('priniting details before redirecting to disaster page');
            console.log(docs);
            res.render('disasterpage', {docs});
        }
    });
});

router.get('/disasterpagee', function (req, res) {
    res.render('disasterpage');
});

// }
module.exports = router;

  

            
// module.exports = 
function CreateTableFromJSON(inputarray) {
    console.log(inputarray);
    console.log("call to the function create table from json");
    var alldisasters = inputarray;
    var col = [];
    for (var i = 0; i < alldisasters.length; i++) {
        for (var key in alldisasters[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    var colhead = document.getElementById("tablehead").innerHTML;
    for (var headdata = 0; headdata < col.length; headdata++) {
        $("#tablehead").append("<th>" + col[headdata] + "</th>");
    }
    for (var i = 0; i < alldisasters.length; i++) {
        var eachrowdata = "<tr>";
        for (var j = 0; j < col.length; j++) {
            var eachrowdata = eachrowdata + "<td>" + myBooks[i][col[j]] + "</td>";
        }
        eachrowdata = eachrowdata + "</tr>"
        $("#rowdata").append(eachrowdata);
    }
}
