package com.example.s528768.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

import java.util.ArrayList;
import java.util.List;


public class BookingDetails extends AppCompatActivity implements ConfirmationDialog.ClearConfirmCallback {
    //    Yeshwanth Rangineni and Kranthi Kumar Parelly have written code for list views to display the booking details.
    ArrayList<Event> event = new ArrayList<Event>();
    String fromtime, totime, roomSelected, dateselected;

    //declaration for display
    String BookingDate,Room,EndTime,StartTime, Username;
    ArrayList<Booking> bookingArrayList = new ArrayList<Booking>();
    BookingAdapter server;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);
        Intent intent=getIntent();
        fromtime= intent.getStringExtra("fromtime"); totime=intent.getStringExtra("totime");
        roomSelected=intent.getStringExtra("roomSelected"); dateselected=intent.getStringExtra("dateselected");
        Log.d(fromtime, "User choosen time from");
        Log.d(totime, "User choosen to time");  Log.d(roomSelected, "User has choosen this room");
        Log.d(dateselected, "User needs a room on this date");
        event.add(new Event(dateselected,fromtime,totime,roomSelected));
//        final ListView lv = (ListView) findViewById(R.id.LvId);
//        final ArrayAdapter<Event> server = new EventAdapter(this,R.layout.activity_event, R.id.date, event);
//        lv.setAdapter(server);


//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                ConfirmationDialog d = new ConfirmationDialog();
//                d.show(getSupportFragmentManager(), "Confirmation");
//
//
//                lv.setAdapter(server);
//            }
//
//        });


        DataQueryBuilder queryBuilder = DataQueryBuilder.create();

        Backendless.Persistence.of(Booking.class).find(queryBuilder, new AsyncCallback<List<Booking>>() {
            @Override
            public void handleResponse(List<Booking> eventObjectsList) {
                Log.d("MYAPP", "Retrieved " + eventObjectsList.size() + " objects");

                if (eventObjectsList.size() > 0) {
                    for (int i = 0; i < eventObjectsList.size(); i++) {
                        Log.d("MYAPP", "Got " + eventObjectsList.get(i) + " object");
                        BookingDate = eventObjectsList.get(i).getBookingDate();
                        Log.d("MYAPP", "Got BookingDate= " + BookingDate);
                        Room = eventObjectsList.get(i).getRoom();
                        Log.d("MYAPP", "Got Room= " + Room);
                        EndTime = eventObjectsList.get(i).getEndTime();
                        Log.d("MYAPP", "Got EndTime= " + EndTime);
                        StartTime = eventObjectsList.get(i).getStartTime();
                        Log.d("MYAPP", "Got StartTime= " + StartTime);

                        //testing
//                    Booking b = new Booking(BookingDate,StartTime,EndTime,Room);
//                    Log.d("booking","Booking object: "+b.toString());

                        // adding to arrayList
                        bookingArrayList.add(new Booking(BookingDate, StartTime, EndTime, Room, Username));
                    }
                    server = new BookingAdapter(getApplicationContext(), R.layout.activity_event, R.id.date, bookingArrayList);
                    final ListView lv = (ListView) findViewById(R.id.LvId);
                    lv.setAdapter(server);
                } else {
                    Toast.makeText(getApplicationContext(), "No entries found", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d( "MYAPP", "Server reported an error " + fault.getMessage() );
            }
        });
    }
    public void setcountref() {
        event.remove(0);
        final ListView lv = (ListView) findViewById(R.id.LvId);
        final ArrayAdapter<Event> server = new EventAdapter(this,R.layout.activity_event, R.id.date, event);
        lv.setAdapter(server);
    }


    public void logout(View v) {
        Intent sub = new Intent(this, MainActivity.class);
        startActivity(sub);
    }
}



