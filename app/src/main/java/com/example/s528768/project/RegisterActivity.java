package com.example.s528768.project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

//Unnathi: Activity to register User
public class RegisterActivity extends AppCompatActivity {
    private EditText userIDField;
    private EditText passwordField;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Button registerbutton=(Button)findViewById(R.id.button19);
        userIDField=(EditText)findViewById(R.id.userid);
        passwordField=(EditText)findViewById(R.id.password);
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userid=userIDField.getText().toString();
                final String password=passwordField.getText().toString();

                //Register a user
                BackendlessUser backendlessUser=new BackendlessUser();
                backendlessUser.setPassword(password);
                backendlessUser.setProperty("UserID",userid);
                Backendless.UserService.register(backendlessUser, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {

                        Toast.makeText(getApplicationContext(),"you registered!",Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getApplicationContext(),"User not registered!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
    //Unnathi : After new register
    public void r1(View v)
    {
        Intent one=new Intent(this, seeroomavailability.class);
        startActivity(one);
    }
}
