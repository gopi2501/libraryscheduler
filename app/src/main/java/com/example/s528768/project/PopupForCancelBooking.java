package com.example.s528768.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class PopupForCancelBooking extends AppCompatActivity {
    //created by unnathi
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_for_cancel_booking);
    }
    public void popup(View v)
    {
        Intent sub=new Intent(this, PopupForCancelBooking.class);
        startActivity(sub);
    }


}
