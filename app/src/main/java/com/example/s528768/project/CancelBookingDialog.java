package com.example.s528768.project;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;

/**
 * Created by s528768 on 10/25/2017.
 */
//yeshwanth created this activity
public class CancelBookingDialog extends DialogFragment {
    public interface ClearConfirmCallback{

        public void clearPositiveCallback();
    }
    public ClearConfirmCallback myactivity;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        myactivity=(ClearConfirmCallback)context;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Do you want to clear count?");
        builder.setMessage("Click Yes if you are..");
        builder.setPositiveButton("Yes",new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                myactivity.clearPositiveCallback();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity();
            }
        });
        return builder.create();
    }

}
