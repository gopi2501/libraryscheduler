package com.example.s528768.project;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by s528759 on 10/27/2017.
 */
//Yashwanth created this activity
public class ConfirmationDialog extends android.support.v4.app.DialogFragment {
    public interface ClearConfirmCallback {
        public void setcountref();
    }

    private ClearConfirmCallback myActivity1;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        myActivity1 = (ClearConfirmCallback) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Booking");
        builder.setMessage("Do you want to delete the booking?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                myActivity1.setcountref();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return builder.create();
    }
}
