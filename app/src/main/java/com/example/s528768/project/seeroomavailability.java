package com.example.s528768.project;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.IDataStore;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.example.s528768.project.R.id.default_activity_button;
import static com.example.s528768.project.R.id.parent;

public class seeroomavailability extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner spinner, spinner1, spinner2;
    String dateselected, fromtime, totime, room;
    String BookingDate,Room,EndTime,StartTime, Username;
    ArrayList<Booking> bookingArrayList = new ArrayList<Booking>();
     BookingAdapter server;
    //  ArrayAdapter<CharSequence> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeroomavailability);
        // Kranthi changed the view and implemented list view
        Intent intent = getIntent();

//
//        IDataStore<Event> eventIDataStore = Backendless.Data.of(Event.class);
//        Log.d("idatastore","Date in iDataStore object : "+eventIDataStore.getObjectCount());
        DataQueryBuilder queryBuilder = DataQueryBuilder.create();

        Backendless.Persistence.of(Booking.class).find(queryBuilder, new AsyncCallback<List<Booking>>() {
            @Override
            public void handleResponse(List<Booking> eventObjectsList) {
                Log.d( "MYAPP", "Retrieved " + eventObjectsList.size() + " objects");

                if(eventObjectsList.size()>0){
                for(int i=0; i<eventObjectsList.size(); i++) {
                    Log.d("MYAPP", "Got " + eventObjectsList.get(i) + " object");
                    BookingDate = eventObjectsList.get(i).getBookingDate();
                    Log.d("MYAPP", "Got BookingDate= " + BookingDate);
                    Room = eventObjectsList.get(i).getRoom();
                    Log.d("MYAPP", "Got Room= " + Room);
                    EndTime =  eventObjectsList.get(i).getEndTime();
                    Log.d("MYAPP", "Got EndTime= " + EndTime);
                    StartTime =  eventObjectsList.get(i).getStartTime();
                    Log.d("MYAPP", "Got StartTime= " + StartTime);

                    //testing
//                    Booking b = new Booking(BookingDate,StartTime,EndTime,Room);
//                    Log.d("booking","Booking object: "+b.toString());

                    // adding to arrayList
                    bookingArrayList.add(new Booking(BookingDate, StartTime, EndTime, Room, Username));
                }
                    server = new BookingAdapter(getApplicationContext(),R.layout.activity_event, R.id.date, bookingArrayList);
                final ListView lv = (ListView) findViewById(R.id.LvId2);
                lv.setAdapter(server);
            }
            else{
                    Toast.makeText(getApplicationContext(),"No entries found",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d( "MYAPP", "Server reported an error " + fault.getMessage() );
            }
        });




        dateselected = intent.getStringExtra("dateSelected");
        System.out.println("Date Selected is " + dateselected);
//hard coded by unnathi

        // adapter = ArrayAdapter.createFromResource(this, R.array.starttime, android.R.layout.simple_spinner_item);
        spinner = (Spinner) findViewById(R.id.spinner);
        String[] items = new String[]{"8AM", "9AM", "10AM", "11AM", "12AM", "1PM", "2PM", "3PM", "4PM", "5PM", "5PM", "6PM", "7PM", "8PM", "9PM", "10PM", "11PM", "12PM"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        String[] items1 = new String[]{"9AM", "10AM", "11AM", "12AM", "1PM", "2PM", "3PM", "4PM", "5PM", "6PM", "7PM", "8PM", "9PM", "10PM", "11PM", "12PM"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items1);
        spinner1.setAdapter(adapter1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        String[] items2 = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items2);
        spinner2.setAdapter(adapter2);

//    Gopi Sai Krishna:    implemented spinner onitemselected for all the three spinners.

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        fromtime = "8AM";
//                        Toast.makeText(parent.getContext(), "9", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        fromtime = "9AM";
                        break;
                    case 2:
                        fromtime = "10AM";
//                        Toast.makeText(parent.getContext(), "Spinner item 3!", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        fromtime = "11AM";
                        break;
                    case 4:
                        fromtime = "12AM";
                        break;
                    case 5:
                        fromtime = "1PM";
                        break;
                    case 6:
                        fromtime = "2PM";
                        break;
                    case 7:
                        fromtime = "3PM";
                        break;
                    case 8:
                        fromtime = "4PM";
                        break;
                    case 9:
                        fromtime = "5PM";
                        break;
                    case 10:
                        fromtime = "6PM";
                        break;
                    case 11:
                        fromtime = "7PM";
                        break;
                    case 12:
                        fromtime = "8PM";
                        break;
                    case 13:
                        fromtime = "9PM";
                        break;
                    case 14:
                        fromtime = "10PM";
                        break;
                    case 15:
                        fromtime = "11PM";
                        break;
                    case 16:
                        fromtime = "12PM";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                fromtime = "8AM";
                // sometimes you need nothing here
            }
        });

//       Gopi Sai Krishna Lemati : have written spinner on item select

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        totime = "9AM";
                        break;
                    case 1:
                        totime = "10AM";
//                        Toast.makeText(parent.getContext(), "Spinner item 3!", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        totime = "11AM";
                        break;
                    case 3:
                        totime = "12AM";
                        break;
                    case 4:
                        totime = "1PM";
                        break;
                    case 5:
                        totime = "2PM";
                        break;
                    case 6:
                        totime = "3PM";
                        break;
                    case 7:
                        totime = "4PM";
                        break;
                    case 8:
                        totime = "5PM";
                        break;
                    case 9:
                        totime = "6PM";
                        break;
                    case 10:
                        totime = "7PM";
                        break;
                    case 11:
                        totime = "8PM";
                        break;
                    case 12:
                        totime = "9PM";
                        break;
                    case 13:
                        totime = "10PM";
                        break;
                    case 14:
                        totime = "11PM";
                        break;
                    case 15:
                        totime = "12PM";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                totime = "9AM";
                // sometimes you need nothing here
            }
        });

//       Gopi Sai Krishna Lemati : have written spinner on item select
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        room = "A";
//                        Toast.makeText(parent.getContext(), "9", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        room = "B";
                        break;
                    case 2:
                        room = "C";
//                        Toast.makeText(parent.getContext(), "Spinner item 3!", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        room = "D";
                        break;
                    case 4:
                        room = "E";
                        break;
                    case 5:
                        room = "F";
                        break;
                    case 6:
                        room = "G";
                        break;
                    case 7:
                        room = "H";
                        break;
                    case 8:
                        room = "I";
                        break;
                    case 9:
                        room = "J";
                        break;
                    case 10:
                        room = "K";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                room = "A";
                // sometimes you need nothing here
            }
        });
    }

    /*
            spinner1 = (Spinner) findViewById(R.id.spinner1);
            String[] items = new String[]{"9AM","10AM", "11AM", "12AM","1PM","2PM"};
            adapter= ArrayAdapter.createFromResource(this, R.array.endtime, android.R.layout.simple_spinner_item);
            spinner.setAdapter(adapter);
            spinner2 = (Spinner) findViewById(R.id.spinner2);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getBaseContext(), parent.getItemIdAtPosition(position) + "selected", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }*/
    public void bookroom(View v) {

        Intent one = new Intent(this, loginpage.class);
//  Gopi Sai Krishna Lemati: have written putExtra for all the variables which can be used ahead in the further pages.
        one.putExtra("fromtime", fromtime);
        one.putExtra("totime", totime);
        one.putExtra("roomSelected", room);
        one.putExtra("dateselected",dateselected);
        startActivity(one);
    }


    public void retrieve(View v) throws Exception {

        IDataStore<Event> eventIDataStore = Backendless.Data.of(Event.class);
        Log.d("idatastore","Date in iDataStore object : "+eventIDataStore.getObjectCount());
        DataQueryBuilder queryBuilder = DataQueryBuilder.create();

        eventIDataStore.find(queryBuilder, new AsyncCallback<List<Event>>() {
            @Override
            public void handleResponse(List<Event> eventObjectsList) {
                Log.d( "MYAPP", "Retrieved " + eventObjectsList.size() + " objects");

                for(int i=0; i<eventObjectsList.size(); i++) {
                    Log.d("MYAPP", "Got " + eventObjectsList.get(i) + " object");
                    BookingDate = eventObjectsList.get(i).getDate();
                    Room = eventObjectsList.get(i).getRoom();
                    EndTime =  eventObjectsList.get(i).getEndTime();
                    StartTime =  eventObjectsList.get(i).getStartTime();

                    Event e = new Event(BookingDate,StartTime,EndTime,Room);
                    Log.d("event","Event object: "+e.toString());
//                    event.add(new Event(BookingDate, StartTime, EndTime, Room));

                }


            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d( "MYAPP", "Server reported an error " + fault.getMessage() );
            }
        });
//        final ArrayAdapter<Event> server = new EventAdapter(this,R.layout.activity_event, R.id.date, event);
        final ListView lv = (ListView) findViewById(R.id.LvId2);
        lv.setAdapter(server);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
