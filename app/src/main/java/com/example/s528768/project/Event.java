package com.example.s528768.project;

import android.app.Activity;

/**
 * Created by s528759 on 10/27/2017.
 */
//Unnathi created this activity
public class Event {
    public String date;
    public String startTime;
    public String endTime;
    public String room;



    public Event(String date, String startTime, String endTime, String room) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.room = room;
    }


    public Event() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }


    @Override
    public String toString() {
        return "Event{" +
                "date='" + date + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", room='" + room + '\'' +
                '}';
    }
}


