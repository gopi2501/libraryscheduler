package com.example.s528768.project;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;


public class loginpage extends AppCompatActivity {
    HashMap Booking = new HashMap();
    String fromtime, totime, room, dateselected;
    private EditText username;

    private EditText password;

    // This code By Yeshwanth
//    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginpage);
//        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        username=(EditText)findViewById(R.id.usernameet);
        password=(EditText)findViewById(R.id.passwordet);
        Intent intent = getIntent();
        fromtime = intent.getStringExtra("fromtime");
        totime = intent.getStringExtra("totime");
        room = intent.getStringExtra("roomSelected");
        dateselected = intent.getStringExtra("dateselected");


    }
    //by yeshwanth to login user
    public void submit(View v) throws Exception{
        EditText idET = (EditText) findViewById(R.id.usernameet);
        EditText passET = (EditText) findViewById(R.id.passwordet);
        final String id = idET.getText().toString();

        Log.d("userid","The user id entered is: "+id);
        String pass = passET.getText().toString();
        Log.d("password","The password entered is: "+pass);
        final Intent one = new Intent(this, BookingDetails.class);


        Backendless.UserService.login(id, pass, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {

                Booking.put("Username",id);
                Booking.put("BookingDate",dateselected);
                Booking.put("EndTime",fromtime);
                Booking.put("StartTime",totime);
                Booking.put("room", room);



                Backendless.Persistence.of( "Booking" ).save( Booking, new AsyncCallback<Map>() {
                    public void handleResponse( Map response )
                    {
                        Log.d("success",id);
                        // new Contact instance has been saved
                    }

                    public void handleFault( BackendlessFault fault )
                    {
                        System.out.println(fault);
                        Log.d("Failure",id);
                        // an error has occurred, the error code can be retrieved with fault.getCode()
                    }
                });

                one.putExtra("username",id);
                one.putExtra("fromtime", fromtime);
                one.putExtra("totime", totime);
                one.putExtra("roomSelected", room);
                one.putExtra("dateselected",dateselected);
                startActivity(one);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }


    //Unnathi: For redirecting to the register page
    public void register(View v) {
        Intent one = new Intent(this, RegisterActivity.class);
        startActivity(one);
    }



    public void clickkkk(View v) {
        Toast.makeText(getApplicationContext(), "clickkkkkkkkkkkk", Toast.LENGTH_SHORT).show();
    }

}


// This code by Kranthi

   /* public void submit(View v) {
        EditText idET = (EditText) findViewById(R.id.editText);
        EditText passET = (EditText) findViewById(R.id.editText2);
        String id = idET.getText().toString();
        Log.d("userid","The user id entered is: "+id);
        String pass = passET.getText().toString();
        Log.d("password","The password entered is: "+pass);

        if(id.equals("admin")&& pass.equals("admin")){
            Intent adminIntent = new Intent(this,Admin.class);
            startActivity(adminIntent);

        } else if(id.equals("user")&& pass.equals("user")){
            Intent one = new Intent(this, BookingDetails.class);
            one.putExtra("fromtime", fromtime);
            one.putExtra("totime", totime);
            one.putExtra("roomSelected", room);
            one.putExtra("dateselected",dateselected);
            startActivity(one);
        } else {
            Toast.makeText(getApplicationContext(), "The entered credentials are wrong", Toast.LENGTH_LONG).show();
        }*/

