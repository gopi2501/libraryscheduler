package com.example.s528768.project;

import android.content.Intent;
import android.icu.util.Calendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.backendless.Backendless;

//created by gopi
public class MainActivity extends AppCompatActivity implements CancelBookingDialog.ClearConfirmCallback{
    //database connections by yeshwanth
    public static final String APPLICATION_ID = "5A76FF34-1AD8-C921-FFDC-3247DA302000";
    public static final String API_KEY = "6658B15D-7AD0-94D6-FF51-456BF039E700";
    public static final String SERVER_URL = "https://api.backendless.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Backendless.initApp(this,APPLICATION_ID,API_KEY);
    }
    //Intent for room availability
    public void roomavailability(View v)
    {
        Intent one=new Intent(this, Calender.class);
        startActivity(one);
    }
    public void yourbookings(View v)
    {
        Intent one=new Intent(this, loginpage.class);
        startActivity(one);
    }
    @Override
    public void clearPositiveCallback(){


    }

}

