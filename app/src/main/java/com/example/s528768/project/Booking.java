package com.example.s528768.project;

/**
 * Created by S528756 on 11/17/2017.
 */

public class Booking {

    public String BookingDate;
    public String EndTime;
    public String StartTime;
    public String room;
    public String Username;


    public Booking(String date, String startTime, String endTime, String room , String username) {
        this.BookingDate = date;
        this.StartTime = startTime;
        this.EndTime = endTime;
        this.room = room;
        this.Username=username;
    }

    public Booking() {
    }

    public String getBookingDate() {
        return BookingDate;
    }

    public void setBookingDate(String bookingDate) {
        BookingDate = bookingDate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "BookingDate='" + BookingDate + '\'' +
                ", EndTime='" + EndTime + '\'' +
                ", StartTime='" + StartTime + '\'' +
                ", room='" + room + '\'' +
                ", Username='" + Username + '\'' +
                '}';
    }
}
