package com.example.s528768.project;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by s528759 on 10/27/2017.
 */
//yeshwanth created this activity
public class EventAdapter extends ArrayAdapter {
    public EventAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull ArrayList<Event> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v= super.getView(position, convertView, parent);
        TextView tv1=(TextView) v.findViewById(R.id.date);
        TextView tv2=(TextView) v.findViewById(R.id.stime);
        TextView tv3=(TextView) v.findViewById(R.id.etime);
        TextView tv4=(TextView) v.findViewById(R.id.room);

        Event item=(Event) getItem(position);
        tv1.setText(item.getDate());
        tv2.setText(item.getStartTime());
        tv3.setText(item.getEndTime());
        tv4.setText(item.getRoom());
        return v;
    }

}
