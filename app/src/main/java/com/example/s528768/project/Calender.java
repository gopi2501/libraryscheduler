package com.example.s528768.project;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;

import java.util.Date;

public class Calender extends AppCompatActivity {
    CalendarView calendar;
    String dateSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        dateSelected="27/10/20";
        Date date = new Date();
        Log.d("date",date.toString());
        calendar = (CalendarView) findViewById(R.id.c);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                dateSelected = dayOfMonth + "/" + month + "/" + year;
                Toast.makeText(getApplicationContext(), dayOfMonth + "/" + month + "/" + year, Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void room(View v) {
        Intent one = new Intent(this, seeroomavailability.class);
//        Gopi Sai Krishna LEmati have modified intent by adding put extra
        one.putExtra("dateSelected", dateSelected);
        startActivity(one);
    }
}

