# README #

Offical URL-- https://bitbucket.org/gopi2501/libraryscheduler
============================
Library Scheduler
============================
This application includes features like checking the availability of the rooms on a specific date and for a specific time. This can cut down the manual effort of the library reception and can even make the student or any resource user plan their study time productively.
============================
Below are the modules included in the application:
Home Page
See Room Availability
Authentication Page
Check your booking
Confirm Booking
Admin Page
=============================
Instructions To use our application--
HOME PAGE-
1) In the Home Page the User Can check the Timings of the library 
2) See Room availability button will navigate to the page where the user can select the date to book a room
3) Check the bookings button will navigate to direct login page so that user can directly check the booking details
CALENDER PAGE-
1) The user can select the date, in order to book the room
SEE ROOM AVAILABLITY-
1) In this page the user can check the Rooms which are already booked. So, that the user can book other room in different timings.
2) In the Drop down menu of start time, the user can select the start time for reserving the room.
3) In the Drop down menu of end time, the user can select the end time of the reserving the room.
4) In the Drop down of room availability, the user can select the room.
5) By clicking on the Book a room button, the page will navigate to the Login page
LOGIN PAGE-
1) The user can enter the login details,i.e 919 ID's, For testing we just gave some sample login details Like
	username: gopi password: gopi
	username: kranthi password: kranthi
	username: unnathi1 password: unnathi1
BOOKING DETAILS-
1) The user can see the booking details of his/her reservation
=============================
Dependencies used for backendless:
com.backendless:backendless:3.0.8.2
=============================
The login credentials to use in the application are:
username: gopi password: gopi
username: kranthi password: kranthi
username: unnathi1 password: unnathi1
=============================


